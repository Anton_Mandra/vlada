<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test</title>
        <link href="../../public/css/bootstrap.css" rel="stylesheet" type="text/css"> 
        <link href="../../public/css/footer.css" rel="stylesheet" type="text/css"> 

        <!-- Fonts -->
      <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">    --> 
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    </head>
    <body  class="d-flex flex-column">   
        @include('layouts.navbar')
        @include('layouts.itemContent')
        @include('layouts.footer')
       
    </body>
</html>
