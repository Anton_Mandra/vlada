<main id="page-content">

    <div class="container">
        <div class="row">
            <div class="col-md">
                <div class="card h-100">

                    <img class="card-img-top" src="../../../public/files/img/{{$item->brandName}}/{{strtolower($item->itemName)}}/{{$item->itemImg}}"
                         alt="{{$item->itemName}}" style="width: 300px">

                    <div class="card-body">
                        <h4 class="card-title">
                            {{$item->itemName}}
                        </h4>
                        <h6 class="card-subtitle mb-2 text-muted">
                            {{$item->brandName}}
                        </h6>
                        <p class="card-text">
                            {{$item->itemDesc}}
                        </p>
                        <div class="text-center">
                            <p>Цена: {{$item->price}} грн</p>
                            <button type="button" class="btn btn-outline-warning" data-itemId="{{$item->itemId}}">В корзину</button>
                        </div>

                    </div>
                </div>
            </div>          
        </div>
    </div>
</main>
