<nav class="navbar fixed-top navbar-dark bg-dark navbar-expand-md">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <a class="navbar-brand" href="{{URL::to('/')}}">My shop</a> 
            </div>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{URL::to('/')}}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Brands
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{URL::to('/brand')}}">All brands</a>
                        <div class="dropdown-divider"></div>
                        @foreach ($brands as $brand)
                        <a class="dropdown-item" href="{{URL::to('/brand').'/'.$brand->brandName}}">{{$brand->brandName}}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categories
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown1">                        
                        @foreach ($categories as $category)
                        <a class="dropdown-item" href="#">{{$category->categoryName}}</a>
                        @endforeach                        
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

