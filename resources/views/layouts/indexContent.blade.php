<main id="page-content">

    <div class="container">
        <div class="row">
            @foreach ($items as $item)
            <div class="col-lg-4 col-sm-6 mb-4">
                <div class="card h-100">
                    <a href="{{URL::to('/item').'/'.$item->itemId}}">
                        <img class="card-img-top" src="../../../public/files/img/{{$item->brandName}}/{{strtolower($item->itemName)}}/{{$item->itemImg}}" alt="{{$item->itemName}}">
                    </a>
                    <div class="card-body">
                        <h4 class="card-title">
                            <a href="{{URL::to('/item').'/'.$item->itemId}}">{{$item->itemName}}</a>
                        </h4>
                        <h6 class="card-subtitle mb-2 text-muted">
                            {{$item->brandName}}
                        </h6>
                        <p class="card-text">
                            {{$item->itemShortDesc}}
                        </p>
                        <div class="text-center">
                            <p>Цена: {{$item->price}} грн</p>
                            <button type="button" class="btn btn-outline-warning" data-itemId="{{$item->itemId}}">В корзину</button>
                        </div>
                        
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</main>
