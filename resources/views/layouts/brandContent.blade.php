<main id="page-content">

    <div class="container">
        <div class="row">
            @foreach ($brands as $brand)
            <div class="col-lg-4 col-sm-6 mb-4">
                <div class="card h-100">
                    <a href="{{URL::to('/brand').'/'.strtolower($brand->brandName)}}">
                        <img class="card-img-top" 
                             src="../../../public/files/img/{{$brand->brandName}}/{{$brand->brandImg}}" alt="{{$brand->brandName}}"
                             >
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</main>
