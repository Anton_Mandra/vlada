<footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
        <small>Copyright &copy; Your Website</small>
    </div>
</footer>
<script src="../../public/js/bootstrap.js"></script>
<script src="../../public/js/bootstrap.bundle.js"></script>
<script src="../../public/js/scripts/contentMarginTop.js"></script>

