<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App;


class MainController extends Controller {
        
    public function index() {
        $categories=App\Category::all();
        $brands=App\Brand::orderBy('brandName')->get();
        $items=DB::table('Items')
                            ->leftJoin('Brands', 'Items.brand','=','Brands.brandId')
                            ->where('stock','>',0)
                            ->get();
        return view('test', ['brands' => $brands,
                             'categories'=>$categories,
                             'items'=>$items]);
    }

}
