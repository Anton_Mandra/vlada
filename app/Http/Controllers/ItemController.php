<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;

class ItemController extends Controller {

    public function index(Request $request) {
        $categories=App\Category::all();
        $brands=App\Brand::orderBy('brandName')->get();
        $item = DB::table('Items')
                ->leftJoin('Brands', 'Items.brand', '=', 'Brands.brandId')
                ->where('itemId', $request->id)
                ->first();
        return view('item', ['brands' => $brands,
                             'categories'=>$categories,
                             'item'=>$item]);
    }

}
