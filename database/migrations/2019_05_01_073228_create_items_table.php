<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Items', function (Blueprint $table) {
            $table->increments('itemId');
            $table->string('brand')
                           ->references('brandId')
                           ->on('Brands');
             $table->string('category')
                          ->references('categoryId')
                          ->on('Categories');
             $table->string('img');
             $table->string('itemName');
             $table->text('itemShortDesc');
             $table->text('itemDesc');
             $table->integer('price');
             $table->integer('stock');
             $table->integer('reserved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
